# dragon_stories_app

This is the naked varsion hack of getting a reasonable sgtoryboard mobile clone running to use based 
on the original storyboard project:

[storyboard](https://github.com/ilikerobots/storyboard)

Orignal credit goes to ileikrobots, Miek Hoolehan

## Overview

React Native Storybook is a way to use BDD to develop Ui components in isolation using 
BDD user stories which results in faster UI design-code time and less errors.

This is the naked version with no Platform Desgin dynamic wdiegt deliver to androd  or ios
so that I can get everything super polished best practices wise and also to 
look into how to implement playing of invidual stories.

# Planning

First, get it set up with my build_envs_app build variants.

Second, integrate it with BMW TEch's Ozzie tech helper

Third, solve the ListView issues that are stil not solved.

Fourth, figure out how to play individual stories.

# Credits

Mike Hoolehan's 

[storyboard](https://github.com/ilikerobots/storyboard)

# License

BSD Clause 2 copyright 2019 Fredirck Grott